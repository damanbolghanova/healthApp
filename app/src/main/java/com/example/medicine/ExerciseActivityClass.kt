package com.example.medicine

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_exercise.*
import java.util.*


class ExerciseActivityClass: AppCompatActivity(), TextToSpeech.OnInitListener{
    private var MY_DATA_CHECK_CODE = 0
    private var restTimer: CountDownTimer?=null
    private var restProgress = 0
    private var count = 0
    private var player: MediaPlayer? = null
    private var tts: TextToSpeech? = null
    private var mSeekBarPitch: SeekBar? = null
    private var mSeekBarSpeed: SeekBar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercise)
        //setSupportActionBar(toolbar_exercise_activity)
        player = MediaPlayer.create(this, R.raw.press_start)
        player!!.isLooping = false // Sets the player to be looping or non-looping.
        player!!.start() // Starts Playback.

        tts = TextToSpeech(this, TextToSpeech.OnInitListener {
            if (it == TextToSpeech.SUCCESS){
                val result = tts!!.setLanguage(Locale.US)
                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED){
                    Log.e("TTS", "Lang is not supported")
                }
            }else{
                Log.e("TTS", "Init Failed")
            }
        })


        /*val checkTTSIntent = Intent()
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);*/


        val actionbar = supportActionBar
        if(actionbar != null){
            actionbar.setDisplayHomeAsUpEnabled(true)
        }
        toolbar_exercise_activity.setNavigationOnClickListener {
            onBackPressed()
        }
        setupRestView(player!!)

    }

    override fun onDestroy() {
        if (restTimer != null){
            restTimer!!.cancel()
            restProgress = 0
        }
        if (tts != null) {
            tts!!.stop()
            tts!!.shutdown()
        }
        super.onDestroy()

    }

    private fun setRestProgressBar(player: MediaPlayer){

        //tts!!.speak("10 seconds rest", TextToSpeech.QUEUE_FLUSH, null, null)

        player.isLooping = false // Sets the player to be looping or non-looping.
        player.start()


        progressBar.progress = restProgress
        restTimer = object : CountDownTimer(5000, 1000){
            override fun onTick(millisUntilFinished: Long){
                restProgress++
                progressBar.progress = 5 - restProgress
                tvTimer.text = (5-restProgress).toString()
            }

            override fun onFinish(){
                Toast.makeText(this@ExerciseActivityClass, "Do Exercise!", Toast.LENGTH_SHORT).show()
                when (count) {
                    0 -> setupActionView0()
                    1 -> setupActionView1()
                    2 -> setupActionView2()
                }
                /*restTimer?.cancel()
                val intent = Intent(this@ExerciseActivityClass, Exercises::class.java)
                startActivity(intent)*/

            }
        }.start()
    }

    private fun setupRestView(player: MediaPlayer){
        if (restTimer != null){
            restTimer!!.cancel()
            restProgress = 0
        }
        player.start()
        textGetReady.text = "GET READY FOR ..."
        exercise_image.visibility = View.INVISIBLE
        setRestProgressBar(player)
    }

    private fun setupActionView0(){
        if (restTimer != null){
            restTimer!!.cancel()
            restProgress = 0
        }
        tvTimer.text = "30"
        progressBar.max = 30
        setAbdominalCrunchProgressBar()
    }

    private fun setAbdominalCrunchProgressBar(){
        tts!!.speak("Abdominal Crunch", TextToSpeech.QUEUE_FLUSH, null, null)
        player = MediaPlayer.create(this, R.raw.press_start)
        player!!.start()
        count++
        textGetReady.text = "Exercise #1"
        exercise_image.setImageResource(R.drawable.ic_abdominal_crunch)
        exercise_image.visibility = View.VISIBLE
        progressBar.progress = restProgress
        restTimer = object : CountDownTimer(15000, 1000){
            override fun onTick(millisUntilFinished: Long) {
                restProgress++
                progressBar.progress = 15-restProgress
                tvTimer.text = (15-restProgress).toString()
            }

            override fun onFinish() {
                tts!!.speak("10 seconds rest", TextToSpeech.QUEUE_FLUSH, null, null)
                Toast.makeText(this@ExerciseActivityClass, "10 sec Rest", Toast.LENGTH_SHORT).show()
                setupRestView(player!!)
            }
        }.start()
    }

    private fun setupActionView1(){
        if (restTimer != null){
            restTimer!!.cancel()
            restProgress = 0
        }
        tvTimer.text = "30"
        progressBar.max = 30
        setPlankProgressBar()
    }

    private fun setPlankProgressBar(){
        tts!!.speak("Plank", TextToSpeech.QUEUE_FLUSH, null, null)
        player = MediaPlayer.create(this, R.raw.press_start)
        player!!.start()
        count++
        textGetReady.text = "Exercise #2"
        exercise_image.setImageResource(R.drawable.ic_plank)
        exercise_image.visibility = View.VISIBLE
        progressBar.progress = restProgress
        restTimer = object : CountDownTimer(30000, 1000){
            override fun onTick(millisUntilFinished: Long) {
                restProgress++
                progressBar.progress = 30-restProgress
                tvTimer.text = (30-restProgress).toString()
            }

            override fun onFinish() {
                tts!!.speak("10 seconds rest", TextToSpeech.QUEUE_FLUSH, null, null)
                Toast.makeText(this@ExerciseActivityClass, "10 sec Rest", Toast.LENGTH_SHORT).show()
                setupRestView(player!!)
            }
        }.start()
    }

    private fun setupActionView2(){
        if (restTimer != null){
            restTimer!!.cancel()
            restProgress = 0
        }
        tvTimer.text = "30"
        progressBar.max = 30
        setJumpingJacksProgressBar()
    }

    private fun setJumpingJacksProgressBar(){
        tts!!.speak("Jumping Jacks", TextToSpeech.QUEUE_FLUSH, null, null)
        player = MediaPlayer.create(this, R.raw.press_start)
        player!!.start()
        count++
        textGetReady.text = "Exercise #3"
        exercise_image.setImageResource(R.drawable.ic_jumping_jacks)
        exercise_image.visibility = View.VISIBLE
        progressBar.progress = restProgress
        restTimer = object : CountDownTimer(30000, 1000){
            override fun onTick(millisUntilFinished: Long) {
                restProgress++
                progressBar.progress = 30-restProgress
                tvTimer.text = (30-restProgress).toString()
            }

            override fun onFinish() {
                tts!!.speak("10 seconds rest", TextToSpeech.QUEUE_FLUSH, null, null)
                Toast.makeText(this@ExerciseActivityClass, "10 sec Rest", Toast.LENGTH_SHORT).show()
                setupRestView(player!!)
            }
        }.start()
    }

    override fun onInit(status: Int) {
        // check for successful instantiation
        if (status == TextToSpeech.SUCCESS) {
            if (tts!!.isLanguageAvailable(Locale.US) == TextToSpeech.LANG_AVAILABLE)
                tts!!.setLanguage(Locale.US);
        } else if (status == TextToSpeech.ERROR) {
            Toast.makeText(this, "Sorry! Text To Speech failed...",
                    Toast.LENGTH_LONG).show();
        }
    }


}