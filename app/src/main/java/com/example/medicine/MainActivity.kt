package com.example.medicine

import android.content.Intent
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        llMainView.setOnClickListener {
            /*var player = MediaPlayer.create(this, R.raw.press_start)
            player!!.isLooping = false
            player.start()*/
            Toast.makeText(this, "Here we start exercise", Toast.LENGTH_SHORT).show()

            val intent = Intent(this, ExerciseActivityClass::class.java)
            startActivity(intent)

        }
    }
}